let path = require('path');
const webpack = require('webpack');
const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer');

let analyzeBundle = process.env.npm_config_bundle_analyze ? true : false;

let getPlugins = mode => {
  let plugins = [
    new webpack.DefinePlugin({
      __DEVELOPMENT__: mode === 'production' ? false : true
    }),
    new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/)
  ];

  if (analyzeBundle) {
    plugins.push(new BundleAnalyzerPlugin({ analyzerMode: 'static' }));
  }

  return plugins;
};

module.exports = (mode = 'production') => ({
  entry: {
    powerup: path.join(__dirname, 'src', 'powerup', 'index.js'),
    view: path.join(__dirname, 'src', 'ticket-view', 'index.js'),
    configuration: path.join(__dirname, 'src', 'configuration', 'index.js')
  },
  output: {
    filename: 'js/[name].js',
    chunkFilename: 'js/[name].js',
    path: path.join(__dirname, 'app'),
    publicPath: './'
  },
  mode: mode,
  plugins: getPlugins(mode),
  module: {
    rules: [
      {
        test: /\.js$/,
        use: [
          {
            loader: 'babel-loader',
            options: {
              presets: [
                [require.resolve('babel-preset-env'), { module: false }],
                require.resolve('babel-preset-react')
              ],
              cacheDirectory: true,
              plugins: [
                ['transform-react-jsx', { pragma: 'h' }],
                'babel-plugin-lodash'
              ]
            }
          }
        ],
        exclude: /node_modules/
      },
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader']
      },
      {
        test: /\.woff2|\.woff$|\.ttf$|\.eot$/,
        use: ['url-loader?limit=1000&name=./fonts/[name].[ext]']
      },
      {
        test: /\.svg$/,
        use: ['url-loader?limit=1&name=./fonts/[name].[ext]']
      }
    ]
  }
});

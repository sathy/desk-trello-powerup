const { request, log } = require('../../utils/index');
const crypto = require('crypto');
const bodyParser = require('body-parser');

let { SLACK_SIGN_SECRET } = process.env;

let DESK_DOMAIN_US = 'https://desk.zoho.com';
let DESK_DOMAIN_EU = 'https://desk.zoho.eu';
let DESK_DOMAIN_CN = 'https://desk.zoho.com.cn';
let DESK_DOMAIN_IN = 'https://desk.zoho.in';

function generateHMACDigest(request) {
  let timestamp = request.headers['x-slack-request-timestamp'];
  let sign_basestring = `v0:${timestamp}:${request.body.toString()}`;
  return sign_basestring;
}

module.exports = function(app) {
  app.post(
    '/api/slackCustomAction',
    bodyParser.raw({ type: 'application/x-www-form-urlencoded' }),
    (req, res) => {
      let body = req.body.toString();
      let validData = true;
      let payload = JSON.parse(decodeURIComponent(body.split('=')[1]));
      const hmac = crypto.createHmac('sha256', SLACK_SIGN_SECRET);
      let hash = `v0=${hmac.update(generateHMACDigest(req)).digest('hex')}`;
      let slack_signature = req.headers['x-slack-signature'];
      if (hash === slack_signature) {
        if (payload.callback_id === 'submit_ticket') {
          let mail = payload.submission.emailId;
          if (
            !mail.match(
              '^[\\w]([\\w\\-\\.\\+\\\'\\/]*)@([\\w\\-\\.]*)(\\.[a-zA-Z]{2,22}(\\.[a-zA-Z]{2}){0,2})$'
            )
          ) {
            validData = false;
            res.json({
              errors: [{ name: 'emailId', error: 'Please enter a valid Email' }]
            });
          }
        }
        if (validData) {
          request({
            url: `${DESK_DOMAIN_US}/support/tpnotify/slackCustomAction?callFrom=AWS`,
            method: 'POST',
            payload: payload
          })
            .then(() => {
              res.status(200).end();
            })
            .catch(err => {
              log(err, '/api/slackCustomAction');
              res
                .status(500)
                .send('Failed to Fetch Slash Custom Action Result in US');
            });
          request({
            url: `${DESK_DOMAIN_EU}/support/tpnotify/slackCustomAction?callFrom=AWS`,
            method: 'POST',
            payload: payload
          })
            .then(() => {
              res.status(200).end();
            })
            .catch(err => {
              log(err, '/api/slackCustomAction');
              res
                .status(500)
                .send('Failed to Fetch Slash Custom Action Result in EU');
            });
          request({
            url: `${DESK_DOMAIN_CN}/support/tpnotify/slackCustomAction?callFrom=AWS`,
            method: 'POST',
            payload: payload
          })
            .then(() => {
              res.status(200).end();
            })
            .catch(err => {
              log(err, '/api/slackCustomAction');
              res
                .status(500)
                .send('Failed to Fetch Slash Custom Action Result in CN');
            });
          request({
            url: `${DESK_DOMAIN_IN}/support/tpnotify/slackCustomAction?callFrom=AWS`,
            method: 'POST',
            payload: payload
          })
            .then(() => {
              res.status(200).end();
            })
            .catch(err => {
              log(err, '/api/slackCustomAction');
              res
                .status(500)
                .send('Failed to Fetch Slash Custom Action Result in IN');
            });
        }
      }
    }
  );
};

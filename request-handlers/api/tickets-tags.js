const express = require('express');
const { request, log } = require('../../utils/index');

let DESK_DOMAIN = 'https://desk.zoho.com';

let deployment = process.env.npm_config_loc_zoho ? 'LOC' : 'IDC';
if (deployment === 'LOC') {
  DESK_DOMAIN = 'https://desk.localzoho.com';
}

module.exports = app => {
  app.post(
    '/api/v1/tickets/tags',
    express.urlencoded({
      extended: true
    }),
    express.json(),
    (req, res) => {
      let ACCESS_TOKEN = req.get('ACCESS_TOKEN');
      let { ticketsInfo } = req.body;
      let { isDescriptionNeeded } = req.query;
      isDescriptionNeeded = JSON.parse(isDescriptionNeeded);

      let tagPromises = ticketsInfo.map(({ ticketId, orgId }) =>
        request({
          url: `${DESK_DOMAIN}/api/v1/tickets/${ticketId}/tags`,
          headers: {
            Authorization: `Zoho-oauthtoken ${ACCESS_TOKEN}`,
            orgId
          }
        })
      );

      let ticketPromises = isDescriptionNeeded
        ? ticketsInfo.map(({ ticketId, orgId }) =>
            request({
              url: `${DESK_DOMAIN}/api/v1/tickets/${ticketId}`,
              headers: {
                Authorization: `Zoho-oauthtoken ${ACCESS_TOKEN}`,
                orgId
              }
            })
          )
        : [Promise.resolve()];

      Promise.all([Promise.all(tagPromises), Promise.all(ticketPromises)])
        .then(([tagResponses, ticketResponses]) => {
          let tags = {};
          let descriptions = {};
          let attachCounts = {};
          tagResponses.forEach((tagResponse, index) => {
            let {
              response: { statusCode }
            } = tagResponse;

            if (statusCode !== 200 && statusCode !== 204) {
              log(tagResponse.body, '/api/v1/tickets/tags');
            }

            if (statusCode === 200) {
              let body = JSON.parse(tagResponse.body);
              tags[ticketsInfo[index].ticketId] = body.tags;
            } else {
              tags[ticketsInfo[index].ticketId] = [];
            }
            if (statusCode !== 200) {
              log(statusCode, tagResponse.body, '/api/v1/tags');
            }
          });

          isDescriptionNeeded &&
            ticketResponses.forEach((ticketResponse, index) => {
              let {
                response: { statusCode }
              } = ticketResponse;

              if (statusCode !== 200 && statusCode !== 204) {
                log(ticketResponse.body, '/api/v1/tickets/tags');
              }

              if (statusCode === 200) {
                let body = JSON.parse(ticketResponse.body);
                let { description, attachmentCount } = body;
                descriptions[ticketsInfo[index].ticketId] = description;
                attachCounts[ticketsInfo[index].ticketId] = attachmentCount;
              } else {
                descriptions[ticketsInfo[index].ticketId] = [];
                attachCounts[ticketsInfo[index].attachmentCount] = 0;
              }
              if (statusCode !== 200) {
                log(statusCode, ticketResponse.body, '/api/v1/tags');
              }
            });

          res.status(200).json({ tags, descriptions, attachCounts });
        })
        .catch(e => {
          log(e, '/api/v1/tags');
          res.status(500).send('Internal Server Error');
        });
    }
  );
};

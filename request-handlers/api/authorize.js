const express = require('express');
const { request, log } = require('../../utils/index');

let {
  DESK_TRELLO_CLIENT_ID,
  DESK_TRELLO_CLIENT_SECRET,
  DESK_TRELLO_CLIENT_DOMAIN
} = process.env;

const REDIRECT_URI = `${DESK_TRELLO_CLIENT_DOMAIN}/api/v1/token`;
const SCOPE = 'Desk.tickets.READ,Desk.basic.READ,Desk.search.READ';

let ACCOUNTS_DOMAIN = 'https://accounts.zoho.com';
let DESK_DOMAIN = 'https://desk.zoho.com';

let deployment = process.env.npm_config_loc_zoho ? 'LOC' : 'IDC';
if (deployment === 'LOC') {
  DESK_TRELLO_CLIENT_ID = process.env.DESK_TRELLO_CLIENT_ID_LOC;
  DESK_TRELLO_CLIENT_SECRET = process.env.DESK_TRELLO_CLIENT_SECRET_LOC;
  ACCOUNTS_DOMAIN = 'https://accounts.localzoho.com';
  DESK_DOMAIN = 'https://deskclient.localzoho.com';
}

module.exports = function(app) {
  app.get('/api/v1/auth', (req, res) => {
    res.redirect(
      301,
      `${ACCOUNTS_DOMAIN}/oauth/v2/auth?response_type=code&client_id=${DESK_TRELLO_CLIENT_ID}&redirect_uri=${REDIRECT_URI}&scope=${SCOPE}&access_type=offline`
    );
  });

  app.get(
    '/api/v1/token',
    express.urlencoded({
      extended: true
    }),
    (req, res) => {
      let { code, error } = req.query;

      if (
        error &&
        typeof error === 'string' &&
        error.toLowerCase() === 'no_org'
      ) {
        res.redirect(`${DESK_DOMAIN}/support/?RefererFrom=Trello`);
        return;
      }

      request({
        url: `${ACCOUNTS_DOMAIN}/oauth/v2/token?code=${code}&client_id=${DESK_TRELLO_CLIENT_ID}&redirect_uri=${REDIRECT_URI}&client_secret=${DESK_TRELLO_CLIENT_SECRET}&grant_type=authorization_code&scope=${SCOPE}`,
        method: 'POST'
      })
        .then(info => {
          let body = JSON.parse(info.body);
          let { access_token, refresh_token, expires_in } = body;

          res.redirect(
            301,
            `/app/authorizeEndPoint.html?oauth=${encodeURIComponent(
              JSON.stringify({
                accessToken: access_token,
                refreshToken: refresh_token,
                expireTime: Date.now() + expires_in,
                expiresIn: expires_in
              })
            )}`
          );
        })
        .catch(err => {
          log(err, '/api/v1/token');
          res.status(500).send('Failed to obtain access token');
        });
    }
  );

  app.post('/api/v1/token', (req, res) => {
    let REFRESH_TOKEN = req.get('REFRESH_TOKEN');

    request({
      url: `${ACCOUNTS_DOMAIN}/oauth/v2/token?refresh_token=${REFRESH_TOKEN}&client_id=${DESK_TRELLO_CLIENT_ID}&client_secret=${DESK_TRELLO_CLIENT_SECRET}&scope=Desk.tickets.READ,Desk.basic.READ&redirect_uri=${REDIRECT_URI}&grant_type=refresh_token`,
      method: 'POST'
    })
      .then(info => {
        let body = JSON.parse(info.body);
        let { access_token, expires_in } = body;

        res.status(200).json({
          accessToken: access_token,
          expireTime: Date.now() + expires_in,
          expiresIn: expires_in
        });
      })
      .catch(err => {
        log(err, '/api/v1/token');
        res.status(500).send('Failed to obtain access token');
      });
  });
};

const { request, log } = require('../../utils/index');

let DESK_DOMAIN = 'https://desk.zoho.com';

let deployment = process.env.npm_config_loc_zoho ? 'LOC' : 'IDC';
if (deployment === 'LOC') {
  DESK_DOMAIN = 'https://desk.localzoho.com';
}

module.exports = app => {
  app.get('/api/v1/organizations', (req, res) => {
    let ACCESS_TOKEN = req.get('ACCESS_TOKEN');

    request({
      url: `${DESK_DOMAIN}/api/v1/organizations`,
      headers: {
        Authorization: `Zoho-oauthtoken ${ACCESS_TOKEN}`
      }
    })
      .then(info => {
        let {
          response: { statusCode }
        } = info;

        if (statusCode !== 200 && statusCode !== 204) {
          log(info.body, '/api/v1/organizations');
        }

        if (statusCode === 200) {
          let body = JSON.parse(info.body);

          res
            .status(200)
            .json(Object.assign({}, body, { count: body.data.length }));
        } else {
          res.status(200).json({ data: [], count: 0 });
        }

        if (statusCode !== 200) {
          log(statusCode, info.body, '/api/v1/organizations');
        }
      })
      .catch(e => {
        log(e, '/api/v1/organizations');
        res.status(500).send('Internal Server Error');
      });
  });
};

const express = require('express');
const { request, log } = require('../../utils/index');

let DESK_DOMAIN = 'https://desk.zoho.com';

let deployment = process.env.npm_config_loc_zoho ? 'LOC' : 'IDC';
if (deployment === 'LOC') {
  DESK_DOMAIN = 'https://desk.localzoho.com';
}

module.exports = app => {
  app.get(
    '/api/v1/tickets/search',
    express.urlencoded({
      extended: true
    }),
    (req, res) => {
      let ACCESS_TOKEN = req.get('ACCESS_TOKEN');
      let { orgId, depId, searchStr } = req.query;

      request({
        url: `${DESK_DOMAIN}/api/v1/tickets/search?departmentId=${depId}&_all=${searchStr}*&limit=10`,
        headers: {
          Authorization: `Zoho-oauthtoken ${ACCESS_TOKEN}`,
          orgId
        }
      })
        .then(info => {
          let {
            response: { statusCode }
          } = info;
          if (statusCode !== 200 && statusCode !== 204) {
            log(info.body, '/api/v1/tickets/search');
          }
          res
            .status(200)
            .json(statusCode === 200 ? JSON.parse(info.body) : { data: [] });
        })
        .catch(e => {
          log(e, '/api/v1/tickets/search');
          res.status(500).send('Failed to obtain tickets');
        });
    }
  );
};

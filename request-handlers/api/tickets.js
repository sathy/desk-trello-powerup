const express = require('express');
const { request, log } = require('../../utils/index');

let DESK_DOMAIN = 'https://desk.zoho.com';

let deployment = process.env.npm_config_loc_zoho ? 'LOC' : 'IDC';
if (deployment === 'LOC') {
  DESK_DOMAIN = 'https://desk.localzoho.com';
}

module.exports = app => {
  app.get(
    '/api/v1/tickets',
    express.urlencoded({
      extended: true
    }),
    (req, res) => {
      let ACCESS_TOKEN = req.get('ACCESS_TOKEN');
      let { orgId, depId, viewId, limits } = req.query;

      limits = Number(limits) + 1;

      request({
        url: `${DESK_DOMAIN}/api/v1/tickets?departmentId=${depId}&viewId=${viewId}&limit=${limits}&sortBy=-recentThread&include=assignee`,
        headers: {
          Authorization: `Zoho-oauthtoken ${ACCESS_TOKEN}`,
          orgId
        }
      })
        .then(info => {
          let {
            response: { statusCode }
          } = info;

          if (statusCode !== 200 && statusCode !== 204) {
            log(info.body, '/api/v1/tickets');
          }

          if (statusCode === 200) {
            let tickets = JSON.parse(info.body);
            let hasMoreTickets = false;
            if (tickets.data.length === limits) {
              hasMoreTickets = true;
              tickets.data.splice(tickets.data.length - 1, 1);
            }
            res.status(200).json(Object.assign(tickets, { hasMoreTickets }));
          } else {
            res.status(200).json({ data: [], hasMoreTickets: false });
          }
        })
        .catch(e => {
          log(e);
          res.status(500).send('Failed to obtain tickets');
        });
    }
  );

  app.post('/api/v1/tickets', express.json(), (req, res) => {
    let ACCESS_TOKEN = req.get('ACCESS_TOKEN');
    let { tickets } = req.body;

    if (Array.isArray(tickets)) {
      let promises = tickets.map(ticket => {
        let { orgId, ticketId } = ticket;
        return request({
          url: `${DESK_DOMAIN}/api/v1/tickets/${ticketId}?include=assignee`,
          headers: {
            Authorization: `Zoho-oauthtoken ${ACCESS_TOKEN}`,
            orgId
          }
        });
      });

      Promise.all(promises)
        .then(ticketsInfo => {
          let tickets = [];
          ticketsInfo.forEach(info => {
            let {
              response: { statusCode }
            } = info;

            if (statusCode !== 200 && statusCode !== 204) {
              log(info.body, '/api/v1/tickets', 'post');
            }

            if (statusCode === 200) {
              tickets.push(JSON.parse(info.body));
            }
          });
          res.status(200).json({ data: tickets });
        })
        .catch(e => {
          log(e);
          res.status(500).send('Failed to obtain some tickets');
        });
    }
  });
};

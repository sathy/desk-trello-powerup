const express = require('express');
const { request, log } = require('../../utils/index');

let DESK_DOMAIN = 'https://desk.zoho.com';

let deployment = process.env.npm_config_loc_zoho ? 'LOC' : 'IDC';
if (deployment === 'LOC') {
  DESK_DOMAIN = 'https://desk.localzoho.com';
}

module.exports = app => {
  app.get(
    '/api/v1/tickets/count',
    express.urlencoded({
      extended: true
    }),
    (req, res) => {
      let ACCESS_TOKEN = req.get('ACCESS_TOKEN');
      let { orgId, depId, viewId } = req.query;

      let promises = [];

      promises.push(
        request({
          url: `${DESK_DOMAIN}/api/v1/myinfo?include=associatedDepartments`,
          headers: {
            Authorization: `Zoho-oauthtoken ${ACCESS_TOKEN}`,
            orgId
          }
        })
      );

      promises.push(
        request({
          url: `${DESK_DOMAIN}/api/v1/tickets/count?departmentId=${depId}&viewId=${viewId}`,
          headers: {
            Authorization: `Zoho-oauthtoken ${ACCESS_TOKEN}`,
            orgId
          }
        })
      );

      Promise.all(promises)
        .then(([deptInfo, countInfo]) => {
          let isAuthorizedDept = false;
          if (deptInfo.response.statusCode === 200) {
            JSON.parse(deptInfo.body).associatedDepartments.forEach(dept => {
              let { id } = dept;
              if (id === depId) {
                isAuthorizedDept = true;
              }
            });
          }
          if (!isAuthorizedDept) {
            res.status(200).json({ count: 0, isAuthorizedDept: false });
            return;
          }
          let {
            response: { statusCode }
          } = countInfo;

          if (statusCode !== 200 && statusCode !== 204) {
            log(countInfo.body, '/api/v1/tickets/count');
          }

          if (statusCode === 200) {
            let counts = JSON.parse(countInfo.body);
            res
              .status(200)
              .json(Object.assign(counts, { isAuthorizedDept: true }));
          } else {
            res.status(200).json({ count: 0, isAuthorizedDept: false });
          }
        })
        .catch(e => {
          log(e);
          res.status(500).send('Failed to obtain tickets count');
        });
    }
  );
};

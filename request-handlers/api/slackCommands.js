const { request, log } = require('../../utils/index');
const crypto = require('crypto');
const bodyParser = require('body-parser');

let { SLACK_SIGN_SECRET } = process.env;

let DESK_DOMAIN_US = 'https://desk.zoho.com';
let DESK_DOMAIN_EU = 'https://desk.zoho.eu';
let DESK_DOMAIN_CN = 'https://desk.zoho.com.cn';
let DESK_DOMAIN_IN = 'https://desk.zoho.in';

function generateHMACDigest(request) {
  let timestamp = request.headers['x-slack-request-timestamp'];
  let sign_basestring = `v0:${timestamp}:${request.body.toString()}`;
  return sign_basestring;
}

module.exports = function(app) {
  app.post(
    '/api/slack',
    bodyParser.raw({ type: 'application/x-www-form-urlencoded' }),
    (req, res) => {
      let body = req.body.toString();
      body = body.split('&').reduce((obj, data) => {
        let [key, value] = data.split('=');
        obj[key] = decodeURIComponent(value);
        return obj;
      }, {});
      const hmac = crypto.createHmac('sha256', SLACK_SIGN_SECRET);
      let hash = `v0=${hmac.update(generateHMACDigest(req)).digest('hex')}`;
      let slack_signature = req.headers['x-slack-signature'];
      if (hash === slack_signature) {
        request({
          url: `${DESK_DOMAIN_US}/support/tpnotify/slack?callFrom=AWS`,
          method: 'POST',
          payload: body
        })
          .then(() => {
            res.status(200).end();
          })
          .catch(err => {
            log(err, '/api/slack');
            res.status(500).send('Failed to Fetch Slash Commands Result in US');
          });

        request({
          url: `${DESK_DOMAIN_EU}/support/tpnotify/slack?callFrom=AWS`,
          method: 'POST',
          payload: body
        })
          .then(() => {
            res.status(200).end();
          })
          .catch(err => {
            log(err, '/api/slack');
            res.status(500).send('Failed to Fetch Slash Commands Result in EU');
          });
        request({
          url: `${DESK_DOMAIN_CN}/support/tpnotify/slack?callFrom=AWS`,
          method: 'POST',
          payload: body
        })
          .then(() => {
            res.status(200).end();
          })
          .catch(err => {
            log(err, '/api/slack');
            res.status(500).send('Failed to Fetch Slash Commands Result in CN');
          });
        request({
          url: `${DESK_DOMAIN_IN}/support/tpnotify/slack?callFrom=AWS`,
          method: 'POST',
          payload: body
        })
          .then(() => {
            res.status(200).end();
          })
          .catch(err => {
            log(err, '/api/slack');
            res.status(500).send('Failed to Fetch Slash Commands Result in IN');
          });
      }
    }
  );
};

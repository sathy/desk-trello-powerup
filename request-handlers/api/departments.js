const express = require('express');
const { request, log } = require('../../utils/index');

let DESK_DOMAIN = 'https://desk.zoho.com';

let deployment = process.env.npm_config_loc_zoho ? 'LOC' : 'IDC';
if (deployment === 'LOC') {
  DESK_DOMAIN = 'https://desk.localzoho.com';
}

module.exports = app => {
  app.get(
    '/api/v1/departments',
    express.urlencoded({
      extended: true
    }),
    (req, res) => {
      let { orgId } = req.query;
      let ACCESS_TOKEN = req.get('ACCESS_TOKEN');

      let promises = [];

      promises.push(
        request({
          url: `${DESK_DOMAIN}/api/v1/myinfo?include=associatedDepartments`,
          headers: {
            Authorization: `Zoho-oauthtoken ${ACCESS_TOKEN}`,
            orgId
          }
        })
      );

      promises.push(
        request({
          url: `${DESK_DOMAIN}/api/v1/departments?isEnabled=true&limit=200`,
          headers: {
            Authorization: `Zoho-oauthtoken ${ACCESS_TOKEN}`,
            orgId
          }
        })
      );

      Promise.all(promises)
        .then(([userDeptRes, allDeptRes]) => {
          let {
            response: { statusCode: userDeptStatus }
          } = userDeptRes;

          let {
            response: { statusCode: allDeptStatus }
          } = allDeptRes;

          if (userDeptStatus !== 200 && userDeptStatus !== 204) {
            log(userDeptRes.body, '/api/v1/departments', 'User departments');
          }

          if (allDeptStatus !== 200 && allDeptStatus !== 204) {
            log(userDeptRes.body, '/api/v1/departments', 'Portal departments');
          }

          let userDepts, allDepts;

          if (userDeptStatus === 200) {
            try {
              userDepts = JSON.parse(userDeptRes.body);
            } catch (e) {
              log(e, 'User depts parse');
            }
          }

          if (allDeptStatus === 200) {
            try {
              allDepts = JSON.parse(allDeptRes.body);
            } catch (e) {
              log(e, 'All depts parse');
            }
          }

          if (userDepts && allDepts) {
            let enabledDepartments = allDepts.data.map(({ id }) => id);

            let departments = userDepts.associatedDepartments.filter(({ id }) =>
              enabledDepartments.includes(id)
            );

            res.status(200).json({
              data: departments,
              timeZone: userDepts.timeZone,
              count: Array.isArray(departments) ? departments.length : 0
            });
          } else {
            res.status(200).json({ data: [], timeZone: '', count: 0 });
          }
        })
        .catch(e => {
          log(e, '/api/v1/departments');
          res.status(500).send('Failed to obtain departments');
        });
    }
  );
};

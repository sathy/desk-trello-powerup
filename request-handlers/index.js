const authorize = require('./api/authorize');
const organization = require('./api/organization');
const departments = require('./api/departments');
const ticketviews = require('./api/ticketviews');
const tickets = require('./api/tickets');
const ticketscount = require('./api/tickets-count');
const ticketstags = require('./api/tickets-tags');
const search = require('./api/search');
const slackCommand = require('./api/slackCommands');
const slackCustom = require('./api/slackCustom');

let handlers = [
  authorize,
  organization,
  departments,
  ticketviews,
  tickets,
  ticketscount,
  ticketstags,
  search,
  slackCommand,
  slackCustom
];

module.exports = app => {
  handlers.forEach(handler => {
    handler(app);
  });
};

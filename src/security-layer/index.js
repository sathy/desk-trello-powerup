let securityLayer = () => {
  /**
   * Check security related conditions here
   */

  //eslint-disable-next-line
  if (__DEVELOPMENT__) {
    if (
      document.referrer === 'https://trello.com/' &&
      window.location.origin === 'https://vasi-zutk79.tsi.zohocorpin.com:6060'
    ) {
      return true;
    }
    return false;
  }

  if (
    document.referrer === 'https://trello.com/' &&
    window.location.origin === 'https://trello.zohodesk.services'
  ) {
    return true;
  }
  return false;
};

export default securityLayer;

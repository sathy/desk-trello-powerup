/*eslint-disable no-param-reassign*/
/*eslint-disable no-useless-escape*/

export let getInfoFromURI = url => {
  let [info] = url.match(/info=.*/);
  info = info.replace(/info=/, '');
  return JSON.parse(window.decodeURIComponent(info));
};

export function getText(html) {
  if (html) {
    let htmlToText = resultHTML => {
      let divElem,
        i,
        aElem = [],
        regExArr = [],
        aTagList = [];
      divElem = document.createElement('div');
      divElem.innerHTML = '';
      resultHTML = resultHTML.replace(/\n+/gi, '');
      resultHTML = resultHTML.replace(/\r+/gi, '');
      resultHTML = resultHTML.replace(/<!--.*?-->/gi, '');
      resultHTML = resultHTML.replace(/<head>.*?<\/head>/gi, '');

      /* special changes for huge attachment link replacement for mail team */

      aElem = resultHTML.match(/<a .*? data-hugatt="hugatt".*?<\/a>/g);
      if (!aElem) {
        aElem = resultHTML.match(/<a .*? data-hugatt="hugatt".*?<\/a>/g);
      }
      // var a = getHugeAttList(_cont);
      if (aElem) {
        for (i = 0; i < aElem.length; i++) {
          divElem.innerHTML += aElem[i];
          aElem[i] = aElem[i].replace(/[\(]/g, '\\(');
          aElem[i] = aElem[i].replace(/[\)]/g, '\\)');
          // aElem[i] = aElem[i].replace(/[\(.*?\)]/g,'\\(\\)');
          regExArr.push(new RegExp(aElem[i]));
          // regExArr.push(aElem[i]);
        }

        aTagList = divElem.getElementsByTagName('a');
        for (i = 0; i < aElem.length; i++) {
          let _list = `${aTagList[i].innerHTML}\n${aTagList[i].getAttribute(
            'href'
          )}`;
          resultHTML = resultHTML.replace(regExArr[i], _list);
        }
      }
      //resultHTML = resultHTML.replace(/<a .*? data-hugatt="hugatt".*?><\/a>/g,_list);
      resultHTML = resultHTML.replace(
        /<div .*? data-hugatt="hugatt".*?><\/div>/g,
        ''
      );

      /* special changes for huge attachment link replacement for mail team */

      resultHTML = resultHTML.replace(/<br.*?>/gi, '\n');
      resultHTML = resultHTML.replace(/<hr.*?>/gi, '\n');
      resultHTML = resultHTML.replace(/<\/p>/gi, '\n');

      // html=html.replace(/<\/div>/gi,'\n');//replace end div tag with \n character
      resultHTML = resultHTML.replace(/<\/h[1-6]>/gi, '\n');
      resultHTML = resultHTML.replace(/<\/tr>/gi, '\n');
      resultHTML = resultHTML.replace(/<\/li>/gi, '\n');

      resultHTML = resultHTML.replace(/<\/td>/gi, ' ');
      resultHTML = resultHTML.replace(/<\/th>/gi, '');

      /* html entities start*/
      // resultHTML = resultHTML.replace(/<\/tr>/gi, '\n');
      // resultHTML = resultHTML.replace(/<br.*?>/gi, '\n');
      resultHTML = resultHTML.replace(/<.*?>/gi, '');
      resultHTML = resultHTML.replace(/&nbsp;/gi, ' ');
      resultHTML = resultHTML.replace(/&lt;/gi, '<');
      resultHTML = resultHTML.replace(/&gt;/gi, '>');
      resultHTML = resultHTML.replace(/&quot;/gi, '"');
      resultHTML = resultHTML.replace(/&amp;/gi, '&');
      resultHTML = resultHTML.replace(/&#39;/gi, '\'');
      resultHTML = resultHTML.replace(/&#x5c;/g, '\\');
      /* html entities end*/
      return resultHTML;
    };

    /* converting reply blockquote to > start*/
    let isBlockQuote = html.search(/<blockquote/gi);
    if (isBlockQuote !== -1) {
      let firstIndex = html.search(/<blockquote/gi);
      let lastIndex = html.toLowerCase().lastIndexOf('</blockquote>');
      let blockquotehtml = html.substring(firstIndex, lastIndex + 13);
      html = html.replace(blockquotehtml, '##===###===##'); // replacing blockquotehtml by this symbol
      blockquotehtml = htmlToText(blockquotehtml);
      blockquotehtml = blockquotehtml.replace(/\n/gi, '\n > ');
      blockquotehtml = ` > ${blockquotehtml}`;
      html = htmlToText(html);
      if (html.search(/##===###===##/gi) !== -1) {
        html = html.replace('##===###===##', blockquotehtml);
      }
      return html;
    } /* converting reply blockquote to > end*/
    return htmlToText(html);
  }
  return '';
}

/* eslint-disable react/prop-types */
/* eslint-disable react/react-in-jsx-scope */
import { h, Component } from 'preact'; //eslint-disable-line no-unused-vars

export default class Icon extends Component {
  constructor(props) {
    super(props);

    this.handleClick = this.handleClick.bind(this);
  }

  shouldComponentUpdate() {
    return true;
  }

  handleClick() {
    let { onClick } = this.props;
    onClick && onClick();
  }

  render() {
    let { id, className = '' } = this.props;

    return (
      <svg className={`icon ${className}`} onClick={this.handleClick}>
        <use xlinkHref={`#${id}`} />
      </svg>
    );
  }
}

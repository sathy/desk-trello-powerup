/* eslint-disable react/prop-types */
/* eslint-disable react/react-in-jsx-scope */
/* eslint-disable react/no-danger */
import { h, Component } from 'preact'; //eslint-disable-line no-unused-vars
import moment from 'moment-timezone';

import { getDepts } from '../../../powerup/api/api';
import Icon from '../Icon/Icon';
import { getText } from '../../../utils';

export default class Ticket extends Component {
  constructor(props) {
    super(props);

    this.state = {
      timeZone: props.hasTimeZone ? props.timeZone : null
    };
  }

  render(props, state) {
    let {
      ticketNumber,
      subject,
      description,
      contact = {},
      createdTime,
      dueDate,
      status,
      webUrl,
      isLastTicket = false,
      tags = {},
      assignee,
      descriptions = {},
      attachmentCount,
      attachCounts = {},
      ticketId,
      id
    } = props;

    let { timeZone } = state;

    let { account = {}, type } = contact;
    let { firstName = '', lastName } = assignee || {};
    let { accountName } = account;

    let currentTicketId = ticketId || id;

    let desc = descriptions[currentTicketId] || description;
    let attachCount = attachCounts[currentTicketId] || attachmentCount;
    attachCount = Number(attachCount);
    desc = getText(desc);
    let currentTicketTags = tags[currentTicketId] || [];
    let slicedTags =
      currentTicketTags.length > 5
        ? currentTicketTags.slice(0, 5)
        : currentTicketTags;

    return (
      <div
        className={'tickets'}
        style={!isLastTicket ? { borderBottom: '1px dotted #d9dce2' } : {}}
      >
        <a href={webUrl} target={'_blank'} className={'dsklinks'}>
          <div className={'tkts'}>
            <div className={'tktid'}>{`#${ticketNumber}`}</div>
            <div className={'tksub'}>{subject}</div>
            {desc && (
              <div
                className={'tkdes'}
                dangerouslySetInnerHTML={{
                  __html: desc
                }}
              />
            )}
            <div className={'tktsub'} title={'Assigned agent'}>
              {(lastName || firstName) && [
                <div
                  className={'tkownr'}
                  key={0}
                >{`${firstName} ${lastName}`}</div>,
                <div className={'dot'} key={1}>
                  .
                </div>
              ]}
              <div className={'tkcont'}>
                {type === 'paid' && (
                  <span className={'icon-paiduser paid'}>
                    <span className={'path1'} />
                    <span className={'path2'} />
                  </span>
                )}
                {type === 'paid' && <div className={'dot'}>.</div>}
                {accountName && <span key={1}>{accountName}</span>}
              </div>
              <div className={'tktime'} title={'Created Time'}>
                <Icon id={'timer'} className={'timerIcon'} />{' '}
                {moment.tz(createdTime, timeZone).format('DD/MM/YY hh:mm:A')}
              </div>
              {dueDate && <div className={'dot'}>.</div>}
              {dueDate && (
                <div className={'tktime'} title={'Due Date'}>
                  <Icon id={'sandClock'} className={'sandClockIcon'} />{' '}
                  {moment.tz(dueDate, timeZone).format('DD/MM/YY hh:mm:A')}
                </div>
              )}
            </div>
            {tags[currentTicketId] && (
              <div>
                {slicedTags.map(({ name }, index) => (
                  <div key={index} className={'tag'}>
                    {name}
                  </div>
                ))}
                {currentTicketTags.length > 5 && (
                  <span className={'moretags'}>{`${currentTicketTags.length -
                    5} more`}</span>
                )}
              </div>
            )}
          </div>

          <div className={'tkstate open'}>
            {attachCount !== 0 &&
              !Number.isNaN(attachCount) && (
                <div className={'rmove'} title={'Attchment count'}>
                  <Icon id={'attachment'} className={'deleteIcon'} />
                  <span className={'atxcount'}>{attachCount}</span>
                </div>
              )}
            {status}
          </div>
        </a>
      </div>
    );
  }

  componentDidMount() {
    let { hasTimeZone } = this.props;
    if (!hasTimeZone) {
      let { orgId } = this.props;
      getDepts(orgId, window.trello).then(({ timeZone }) => {
        this.setState({ timeZone });
      });
    }
  }
}

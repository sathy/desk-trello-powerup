/* eslint-disable react/prop-types */
/* eslint-disable react/react-in-jsx-scope */
import { h, Component } from 'preact'; //eslint-disable-line no-unused-vars

import IconContent from '../../components/IconContent/IconContent';
import Ticket from '../../components/Ticket/Ticket';

import { getMassTickets, getTicketsTags } from '../../../powerup/api/api';

let trello = window.TrelloPowerUp.iframe();

export default class TicketsContainer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      tickets: [],
      tags: {}
    };

    this.setContainerRef = this.setContainerRef.bind(this);
  }

  setContainerRef(node) {
    this.container = node;
  }

  render(props, state) {
    let { tickets, tags } = state;
    return (
      <div className={'container'} ref={this.setContainerRef}>
        <IconContent />
        {tickets.map((ticket, index) => {
          let isLastTicket = index === tickets.length - 1;
          return (
            <Ticket
              key={index}
              {...ticket}
              {...ticket.info}
              isLastTicket={isLastTicket}
              hasTimeZone={false}
              tags={tags}
            />
          );
        })}
      </div>
    );
  }

  componentDidMount() {
    let { tickets } = this.props;
    getMassTickets(tickets.map(ticket => ticket.info), window.trello).then(
      ({ data }) => {
        //eslint-disable-next-line
        data = data.map((ticket, index) => {
          return Object.assign({}, ticket, tickets[index]);
        });
        //eslint-disable-next-line
        let availableTickets = (data = data
          .filter(ticket => !ticket.isDeleted)
          .reverse());
        let ticketsInfo = data
          .reverse()
          .map(
            (ticket, index) =>
              !ticket.isDeleted
                ? {
                    ticketId: tickets[index].info.ticketId,
                    orgId: tickets[index].info.orgId
                  }
                : false
          )
          .filter(ticket => ticket);
        this.setState({ tickets: availableTickets }, () => {
          getTicketsTags(ticketsInfo, window.trello).then(({ tags }) => {
            this.setState({ tags });
          });
        });
      }
    );
    let { offsetHeight } = this.container;
    this.lastHeight = offsetHeight;
    trello.sizeTo(offsetHeight);
  }

  componentDidUpdate() {
    let { offsetHeight } = this.container;
    if (offsetHeight !== this.lastHeight) {
      trello.sizeTo(offsetHeight);
    }
  }
}

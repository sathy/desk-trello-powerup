/* eslint-disable react/prop-types */
/* eslint-disable react/react-in-jsx-scope */
import { h, Component } from 'preact'; //eslint-disable-line no-unused-vars

import Ticket from '../../components/Ticket/Ticket';
import IconContent from '../../components/IconContent/IconContent';

import {
  getTickets,
  getTicketsCount,
  getTicketsTags
} from '../../../powerup/api/api';
import { getI18nValue } from '../../../i18n/i18n';

let trello = window.TrelloPowerUp.iframe();

export default class TicketViewContainer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      count: null,
      tags: {},
      descriptions: {},
      attachCounts: {}
    };

    this.setContainerRef = this.setContainerRef.bind(this);
  }

  setContainerRef(node) {
    this.container = node;
  }

  render(props, state) {
    let { tickets, tags, descriptions, attachCounts } = state;
    let {
      timeZone,
      viewInfo: { viewURL, orgName, deptName }
    } = props;
    return (
      <div className={'container'} ref={this.setContainerRef}>
        <IconContent />
        <div className={'port'} title={'Portal'}>
          {orgName}
        </div>
        <span className={'sprt'}> / </span>
        <div className={'port'} title={'Department'}>
          {deptName}
        </div>
        {Array.isArray(tickets) && tickets.length ? (
          tickets.map((ticket, index) => {
            let isLastTicket = index === tickets.length - 1;
            return (
              <Ticket
                key={index}
                {...ticket}
                timeZone={timeZone}
                isLastTicket={isLastTicket}
                hasTimeZone={true}
                tags={tags}
                descriptions={descriptions}
                attachCounts={attachCounts}
              />
            );
          })
        ) : Array.isArray(tickets) ? (
          <div>{getI18nValue('no.tickets.found')}</div>
        ) : (
          <div>{getI18nValue('loading')}</div>
        )}
        <a href={viewURL} target={'_blank'} className={'mreIcn'}>
          {getI18nValue('open.view.in.zoho.desk')}
        </a>
      </div>
    );
  }

  componentDidMount() {
    let {
      viewInfo: { orgId, deptId, viewId }
    } = this.props;
    getTickets(orgId, deptId, viewId, trello, 5).then(res => {
      let { data: tickets, hasMoreTickets } = res;

      let ticketsInfo = tickets
        .map(({ id }) => ({
          ticketId: id,
          orgId
        }))
        .filter(ticket => ticket);
      this.setState({ tickets, hasMoreTickets }, () => {
        getTicketsTags(ticketsInfo, window.trello, true).then(
          ({ tags, descriptions, attachCounts }) => {
            this.setState({ tags, descriptions, attachCounts });
          }
        );
      });
    });

    getTicketsCount(orgId, deptId, viewId, trello).then(({ count }) => {
      this.setState({ count });
    });

    let { offsetHeight } = this.container;
    this.lastHeight = offsetHeight;
    trello.sizeTo(offsetHeight);
  }

  componentDidUpdate() {
    let { offsetHeight } = this.container;
    if (offsetHeight !== this.lastHeight) {
      trello.sizeTo(offsetHeight);
    }
  }
}

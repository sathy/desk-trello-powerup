/* eslint-disable react/prop-types */
/*eslint-disable*/
import securityLayer from '../security-layer';
import { h, render } from 'preact';

import TicketViewContainer from './containers/TicketViewContainer/TicketViewContainer';
import TicketsContainer from './containers/TicketsContainer/TicketsContainer';

import { getTickets } from '../powerup/api/api';
import { getInfoFromURI } from '../utils';

import './css/view.css';
import './css/webfonts.css';

if (securityLayer()) {
  window.trello = window.TrelloPowerUp.iframe();
  let views = document.getElementById('views');
  let isMemberSignedIn = trello.isMemberSignedIn();

  let isTicketsPage = window.location.search.indexOf('isTicket') !== -1;

  let viewInfo,
    isInitialRender = true,
    persistedTicketsLength = -1;
  if (!isTicketsPage) {
    viewInfo = getInfoFromURI(window.location.search);
  }

  trello.render(() => {
    trello
      .get('member', 'private', 'isAuthorized', false)
      .then(isAuthorized => {
        if (isMemberSignedIn && isAuthorized) {
          if (isTicketsPage) {
            trello
              .card('attachments')
              .get('attachments')
              .filter(attachment => attachment.url.indexOf('isTicket') !== -1)
              .then(attachments => {
                let tickets = attachments.map(({ url, id }) => {
                  return { info: getInfoFromURI(url), id };
                });

                if (persistedTicketsLength !== tickets.length) {
                  views.innerHTML = '';
                  render(<TicketsContainer tickets={tickets} />, views);
                  persistedTicketsLength = tickets.length;
                }
                if (isInitialRender) {
                  isInitialRender = false;
                }
              });
          } else {
            if (isInitialRender) {
              render(<TicketViewContainer viewInfo={viewInfo} />, views);
            }

            if (isInitialRender) {
              isInitialRender = false;
            }
          }
        }
      });
  });
} else {
  let { redirectURI } = getInfoFromURI(window.location.search);
  window.location.href = redirectURI;
}

/* eslint-disable react/prop-types */
/* eslint-disable react/react-in-jsx-scope */
import { h, Component, render } from 'preact'; //eslint-disable-line no-unused-vars

import { getOrgs } from '../../../powerup/api/api';
import { getI18nValue } from '../../../i18n/i18n';

export default class OrganizationPopup extends Component {
  constructor(props) {
    super(props);

    this.state = {
      organizations: [],
      normalizedOrgs: {}
    };

    this.handleOrgChange = this.handleOrgChange.bind(this);
  }

  handleOrgChange(event) {
    let {
      target: { value }
    } = event;
    let { normalizedOrgs } = this.state;
    let orgInfo = normalizedOrgs[value];
    let { setDefaultOrg } = this.props;
    let { id, organizationName, portalURL } = orgInfo;
    setDefaultOrg({ id, organizationName, portalURL });
  }

  render(props, state) {
    let { organizations } = state;
    let { orgInfo = {} } = props;
    let selectedOrg = orgInfo.id;

    return (
      <select
        className={'trdrp'}
        value={selectedOrg ? selectedOrg : 'default'}
        onChange={this.handleOrgChange}
        selected={'Hello'}
      >
        {organizations.map((organization, index) => {
          let { organizationName, id } = organization;
          return (
            <option key={index} value={id}>
              {organizationName}
            </option>
          );
        })}
        <option style={{ display: 'none' }} value={'default'}>
          {getI18nValue('no.portal.selected')}
        </option>
      </select>
    );
  }

  componentDidMount() {
    getOrgs(window.trello).then(({ data }) => {
      let normalizedOrgs = data.reduce((orgs, org) => {
        let { id } = org;
        orgs[id] = org;
        return orgs;
      }, {});
      this.setState({ organizations: data, normalizedOrgs });
    });
  }
}

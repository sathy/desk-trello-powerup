/* eslint-disable react/prop-types */
/* eslint-disable react/react-in-jsx-scope */
import { h, Component, render } from 'preact'; //eslint-disable-line no-unused-vars

import { getDepts } from '../../../powerup/api/api';
import { getI18nValue } from '../../../i18n/i18n';

export default class DepartmentsPopup extends Component {
  constructor(props) {
    super(props);

    this.state = {
      departments: [],
      normalizedDepts: {}
    };

    this.handleDeptChange = this.handleDeptChange.bind(this);
    this.getDepartments = this.getDepartments.bind(this);
  }

  handleDeptChange(event) {
    let {
      target: { value }
    } = event;
    let { normalizedDepts } = this.state;
    let deptInfo = normalizedDepts[value];
    let { setDefaultDept } = this.props;
    let { id, name } = deptInfo;
    setDefaultDept({ id, name });
  }

  getDepartments() {
    let { orgInfo = {} } = this.props;
    let { id } = orgInfo;
    if (id) {
      getDepts(id, window.trello).then(({ data }) => {
        let normalizedDepts = data.reduce((depts, dept) => {
          let { id } = dept;
          depts[id] = dept;
          return depts;
        }, {});
        this.setState({ departments: data, normalizedDepts, isLoading: false });
      });
    }
  }

  render(props, state) {
    let { departments, isLoading } = state;
    let { deptInfo = {}, orgInfo } = props;
    let selectedDept = deptInfo.id;

    return (
      <select
        className={'trdrp'}
        value={selectedDept ? selectedDept : isLoading ? 'loading' : 'default'}
        onChange={this.handleDeptChange}
        disabled={!orgInfo}
      >
        {departments.map((department, index) => {
          let { name, id } = department;
          return (
            <option key={index} value={id}>
              {name}
            </option>
          );
        })}
        <option style={{ display: 'none' }} value={'default'}>
          {getI18nValue('no.department.selected')}
        </option>
      </select>
    );
  }

  componentDidMount() {
    this.getDepartments();
  }

  componentDidUpdate(preProps) {
    let { orgInfo = {} } = this.props;
    let { id } = orgInfo;

    let { orgInfo: preOrgInfo = {} } = preProps;
    let { id: preId } = preOrgInfo;
    if (preId !== id) {
      this.getDepartments();
    }
  }
}

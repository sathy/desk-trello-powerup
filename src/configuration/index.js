/* eslint-disable react/prop-types */
/* eslint-disable react/react-in-jsx-scope */
import securityLayer from '../security-layer';
import { h, Component, render } from 'preact'; //eslint-disable-line no-unused-vars

import './css/webfonts.css';
import './css/configuration.css';
import OrganizationPopup from './components/OrganizationPopup/OrganizationPopup';
import DepartmentsPopup from './components/DepartmentsPopup/DepartmentsPopup';

import { setData, getData } from '../powerup/utils';
import { getI18nValue } from '../i18n/i18n';

if (securityLayer()) {
  window.trello = window.TrelloPowerUp.iframe();

  class ConfigurationPopup extends Component {
    constructor(props) {
      super(props);
      this.state = {
        config: {},
        hasDefaultEntities: false
      };

      this.setDefaultOrg = this.setDefaultOrg.bind(this);
      this.setDefaultDept = this.setDefaultDept.bind(this);
      this.setDefaultConfig = this.setDefaultConfig.bind(this);
      // this.handleReset = this.handleReset.bind(this);
    }

    setDefaultOrg(orgInfo) {
      let { config } = this.state;

      this.setState({
        config: Object.assign({}, config, { orgInfo, deptInfo: {} }),
        hasDefaultEntities: false
      });
    }

    setDefaultDept(deptInfo) {
      let {
        config,
        config: { orgInfo }
      } = this.state;

      this.setState({
        config: Object.assign({}, config, { orgInfo, deptInfo }),
        hasDefaultEntities: false
      });
    }

    setDefaultConfig() {
      let {
        config: { orgInfo, deptInfo },
        hasDefaultEntities
      } = this.state;

      if (!orgInfo && !deptInfo) {
        return;
      }

      if (hasDefaultEntities) {
        setData(window.trello, 'config', {}).then(() => {
          setData(window.trello, 'hasDefaultEntities', false).then(() => {
            this.setState({ hasDefaultEntities: false, config: {} });
          });
        });
      } else {
        if (orgInfo || deptInfo) {
          getData(window.trello, 'config', {}).then(config =>
            setData(
              window.trello,
              'config',
              Object.assign({}, config, {
                orgInfo: orgInfo || {},
                deptInfo: deptInfo || {}
              })
            ).then(() => {
              setData(window.trello, 'hasDefaultEntities', true).then(() => {
                window.trello.back();
              });
            })
          );
        }
      }
    }

    // handleReset() {
    //   removeData(window.trello, 'config').then(() => {
    //     this.setState({ config: {} });
    //   });
    // }

    render(props, state) {
      let {
        config: { orgInfo, deptInfo },
        hasDefaultEntities
      } = state;
      return (
        <div>
          <div>
            <div className='js-results'>
              <div className='dpscs'>
                <div className='tlbl'>{getI18nValue('choose.portal')}</div>
                <div className='drpprnt'>
                  <OrganizationPopup
                    orgInfo={orgInfo}
                    setDefaultOrg={this.setDefaultOrg}
                  />
                </div>
              </div>
              {
                <div className='dpscs1'>
                  <div className='tlbl'>
                    {getI18nValue('choose.department')}
                  </div>
                  <div className='drpprnt'>
                    <DepartmentsPopup
                      orgInfo={orgInfo}
                      deptInfo={deptInfo}
                      setDefaultDept={this.setDefaultDept}
                    />
                  </div>
                </div>
              }
              <div className='root'>
                <span className='reset' onClick={this.setDefaultConfig}>
                  <div
                    className={
                      orgInfo && deptInfo ? 'revoke' : 'revoke disabled'
                    }
                  >
                    {hasDefaultEntities
                      ? getI18nValue('revoke.defaults')
                      : getI18nValue('set.as.default')}
                  </div>
                </span>

                {/* <span className='reset' onClick={this.handleReset}>
                  <input
                    type='button'
                    name=''
                    className='revoke'
                    value='Reset Configs'
                  />
                </span> */}
              </div>
            </div>
          </div>
        </div>
      );
    }

    componentDidMount() {
      let promises = [
        window.trello.get('member', 'private', 'config', {}),
        window.trello.get('member', 'private', 'hasDefaultEntities', false)
      ];

      Promise.all(promises).then(([config, hasDefaultEntities]) => {
        this.setState({ config, hasDefaultEntities });
      });
    }
  }

  render(<ConfigurationPopup />, document.getElementById('target'));
}

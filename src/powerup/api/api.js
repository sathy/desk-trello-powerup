import { get } from 'lodash-es';

import { fetch } from '../request/request';
import { getState, setState, clearState } from '../state';
import { setData, getData } from '../utils';

let request = (schema, trello) =>
  new Promise((resolve, reject) => {
    let {
      domain,
      path,
      params = {},
      method = 'GET',
      headers = {},
      payload = null,
      statePath,
      shouldAttachAccessToken = true,
      shouldCallAPI = () => true
    } = schema;

    let state = getState();
    if (!shouldCallAPI(state, statePath)) {
      return getData(trello, 'isAuthorized', false).then(isAuthorized => {
        if (!isAuthorized) {
          clearState();
          state = {};
        }
        return resolve(statePath ? get(state, statePath) : {});
      });
    }

    let oauthPromise;
    if (trello) {
      oauthPromise = getData(trello, 'oauth', false);
    } else {
      oauthPromise = Promise.resolve({});
    }

    return oauthPromise.then(oauth => {
      if (!oauth) {
        throw new Error('Failed to obtain oauth');
      }
      let { expireTime, refreshToken, accessToken } = oauth;

      let refreshPromise;
      if (
        Date.now() + 300000 >= expireTime &&
        refreshToken &&
        expireTime &&
        trello
      ) {
        // Refresh access token in 5 mins earlier
        refreshPromise = refreshAuthToken(refreshToken);
      } else {
        refreshPromise = Promise.resolve(oauth);
      }

      return refreshPromise.then(
        ({ accessToken: latestAccessToken, expiresIn, expireTime }) => {
          let querystr = Object.keys(params)
            .map(param => `${param}=${encodeURIComponent(params[param])}`)
            .join('&');

          if (latestAccessToken !== accessToken && trello) {
            setData(trello, 'oauth', {
              accessToken: latestAccessToken,
              expiresIn,
              expireTime,
              refreshToken
            });
          }

          return fetch(`${domain}${path}${querystr ? `?${querystr}` : ''}`, {
            headers: Object.assign(
              {},
              headers,
              {
                PARENT_ORIGIN: document.referrer
              },
              shouldAttachAccessToken
                ? { ACCESS_TOKEN: latestAccessToken }
                : {},
              payload ? { 'Content-Type': 'application/json' } : {}
            ),
            method,
            body: payload && JSON.stringify(payload)
          })
            .then(res => {
              let { status } = res;

              if (status === 200) {
                res
                  .json()
                  .then(data => {
                    if (statePath) {
                      setState(statePath, data);
                    }
                    resolve(data);
                  })
                  .catch(err => {
                    reject(err);
                  });
              } else {
                resolve({ status });
              }
            })
            .catch(err => {
              reject(err);
            });
        }
      );
    });
  });

let shouldCallAPI = (state, path) => {
  let entity = get(state, path);
  if (entity === null || entity === undefined) {
    return true;
  }
  return false;
};

let refreshAuthToken = refreshToken => {
  let schema = {
    domain: '',
    path: '/api/v1/token',
    headers: { REFRESH_TOKEN: refreshToken },
    method: 'POST',
    shouldAttachAccessToken: false
  };
  return request(schema, false);
};

export let getOrgs = trello => {
  let schema = {
    domain: '',
    path: '/api/v1/organizations',
    statePath: 'orgs',
    shouldCallAPI: shouldCallAPI
  };
  return request(schema, trello);
};

export let getDepts = (orgId, trello) => {
  let schema = {
    domain: '',
    path: '/api/v1/departments',
    params: { orgId },
    statePath: `depts.${orgId}`,
    shouldCallAPI: shouldCallAPI
  };

  return request(schema, trello);
};

export let getTicketViews = (orgId, depId, trello) => {
  let schema = {
    domain: '',
    path: '/api/v1/ticketviews',
    params: { depId, orgId },
    statePath: `ticketViews.${orgId}.${depId}`,
    shouldCallAPI: shouldCallAPI
  };
  return request(schema, trello);
};

export let getTickets = (orgId, depId, viewId, trello, limits) => {
  let schema = {
    domain: '',
    path: '/api/v1/tickets',
    params: { orgId, depId, viewId, limits }
  };
  return request(schema, trello);
};

export let getMassTickets = (tickets, trello) => {
  let schema = {
    domain: '',
    method: 'POST',
    path: '/api/v1/tickets',
    payload: { tickets }
  };
  return request(schema, trello);
};

export let getTicketsCount = (orgId, depId, viewId, trello) => {
  let schema = {
    domain: '',
    path: '/api/v1/tickets/count',
    params: { orgId, depId, viewId }
  };
  return request(schema, trello);
};

export let getTicketsTags = (
  ticketsInfo,
  trello,
  isDescriptionNeeded = false
) => {
  let schema = {
    domain: '',
    path: '/api/v1/tickets/tags',
    params: { isDescriptionNeeded },
    payload: { ticketsInfo },
    method: 'POST'
  };
  return request(schema, trello);
};

export let searchTickets = (orgId, depId, _, trello, searchStr) => {
  let schema = {
    domain: '',
    path: '/api/v1/tickets/search',
    params: { orgId, depId, searchStr }
  };
  return request(schema, trello);
};

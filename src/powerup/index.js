import securityLayer from '../security-layer';

import MenuPopup from './popups/MenuPopup';
import { authorize } from './authorization/AuthorizePopup';
import ViewAttachment from './attach/ViewAttachment';

import { getData } from './utils';

if (securityLayer()) {
  window.TrelloPowerUp.initialize({
    'card-buttons': MenuPopup,
    'attachment-sections': ViewAttachment,
    'authorization-status': trello =>
      getData(trello, 'isAuthorized', false).then(isAuthorized => ({
        authorized: isAuthorized
      })),
    'show-authorization': trello => {
      authorize(trello, false);
      return trello.closePopup();
    },
    'card-badges': (trello, options) => {
      let { attachments } = options;
      let tickets = attachments.filter(attachment => {
        let { url } = attachment;

        return (
          url.indexOf(`${window.location.origin}/app/view.html`) !== -1 &&
          url.indexOf('isTicket') !== -1
        );
      });

      if (!tickets.length) {
        return [];
      }

      return [
        {
          text: tickets.length,
          icon: `${window.location.origin}/icon?color=999`
        }
      ];
    }
  });
}

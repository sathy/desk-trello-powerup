let securityLayer = () => 
  /**
   * Check security related conditions here
   */
  true
;

export default securityLayer;

import { closePopup } from '../utils';

export default (orgInfo, deptInfo, viewInfo, ticketInfo) => trello => {
  let { id, ticketNumber, subject, webUrl } = ticketInfo;

  let { id: orgId } = orgInfo;
  let { id: deptId } = deptInfo;
  let { id: viewId } = viewInfo;

  trello.attach({
    name: `#${ticketNumber} ${subject}`,
    url: `${window.location.origin}/app/view.html?info=${encodeURIComponent(
      JSON.stringify({
        orgId,
        ticketId: id,
        isTicket: true,
        deptId,
        viewId,
        redirectURI: webUrl
      })
    )}`
  });

  return closePopup(trello);
};

import { icon } from '../manifest.json';
import { getInfoFromURI } from '../../utils';
import { getData } from '../utils';
import { getTicketsCount, getMassTickets } from '../api/api';

export default (trello, options) => {
  let isMemberSignedIn = trello.isMemberSignedIn();
  return getData(trello, 'isAuthorized').then(isAuthorized => {
    if (isMemberSignedIn && isAuthorized) {
      let [tickets, views] = options.entries.reduce(
        (claimed, attachment) => {
          let { url } = attachment;
          if (url.indexOf(`${window.location.origin}/app/view.html`) !== -1) {
            if (url.indexOf('isTicket') !== -1) {
              claimed[0].push(attachment);
            } else {
              claimed[1].push(attachment);
            }
          }
          return claimed;
        },
        [[], []]
      );

      let claimed = [''];
      let claimedTickets = [];
      let promises = [];

      if (tickets && tickets.length > 0) {
        tickets.forEach(claim => {
          let { orgId, ticketId } = getInfoFromURI(claim.url);
          promises.push(
            getMassTickets([{ orgId, ticketId }], trello).then(
              ({ data = [] }) => {
                let { isDeleted = true } = data[0] || {};
                if (!isDeleted) {
                  claimedTickets.push(claim);
                }
                return null;
              }
            )
          );
        });
      }

      if (views && views.length > 0) {
        views.forEach(claim => {
          let { viewName, orgId, deptId, viewId } = getInfoFromURI(claim.url);
          promises.push(
            getTicketsCount(orgId, deptId, viewId, trello).then(
              ({ count, isAuthorizedDept }) => {
                if (isAuthorizedDept) {
                  claimed.push({
                    claimed: [claim],
                    icon: `${window.location.origin}${icon}`,
                    title: `${viewName.toUpperCase()} ( ${count} )`,
                    content: {
                      type: 'iframe',
                      url: trello.signUrl(claim.url),
                      height: 200
                    }
                  });
                }
              }
            )
          );
        });
      } else {
        promises.push(Promise.resolve());
      }

      return Promise.all(promises).then(() => {
        if (claimedTickets.length) {
          claimed[0] = {
            claimed: claimedTickets,
            icon: `${window.location.origin}${icon}`,
            title: 'Zoho Desk Tickets',
            content: {
              type: 'iframe',
              url: trello.signUrl(
                `${window.location.origin}/app/view.html?isTicket=true`
              ),
              height: 200
            }
          };
        } else {
          claimed.splice(0, 1);
        }
        return claimed;
      });
    }
    return [];
  });
};

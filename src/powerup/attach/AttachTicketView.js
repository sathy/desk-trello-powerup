import { closePopup } from '../utils';

export default (orgInfo, deptInfo, viewInfo, timeZone) => trello => {
  let { name, id: viewId } = viewInfo;
  let { id: orgId, portalURL, organizationName } = orgInfo;
  let { id: deptId, name: deptName } = deptInfo;
  trello.attach({
    name,
    url: `${window.location.origin}/app/view.html?info=${encodeURIComponent(
      JSON.stringify({
        orgId,
        deptId,
        viewId,
        viewName: name,
        viewURL: `${portalURL}#Cases/lv/${viewId}`,
        orgName: organizationName,
        deptName,
        timeZone,
        redirectURI: `${portalURL}#Cases/lv/${viewId}`
      })
    )}`
  });

  return closePopup(trello);
};

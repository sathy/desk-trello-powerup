import AttachTicketsPopup from '../popups/AttachTicketsPopup';
import AttachTicketViewPopup from '../popups/AttachTicketViewPopup';
import ConfigurationPopup from '../popups/ConfigurationPopup';

export default [AttachTicketsPopup, AttachTicketViewPopup, ConfigurationPopup];

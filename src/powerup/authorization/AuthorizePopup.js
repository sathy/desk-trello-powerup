import { setDatum, createPopup } from '../utils';
import { setState, clearState } from '../state';

import { AUTH_URL } from '../manifest.json';
import { getI18nValue } from '../../i18n/i18n';

export let authorize = (trello, willClosePopup = true) =>
  new Promise(resolve => {
    let width = 700;
    let height = 700;
    let left = window.screen.width / 2 - width / 2;
    let top = window.screen.height / 2 - height / 2;

    let authWindow = window.open(
      `${AUTH_URL}`,
      'Authorization Page',
      `width=${width},height=${height},top=${top},left=${left}`
    );

    window.confirmAuthrize = oauth => {
      setDatum(trello, { isAuthorized: true, oauth });

      clearState();
      setState('isAuthorized', true);
      authWindow.close();
      window.confirmAuthrize = undefined;
      resolve();
    };
  }).then(() => {
    if (willClosePopup) {
      return trello.closePopup();
    }
    return;
  });

export default trello =>
  createPopup(trello, {
    title: getI18nValue('auth'),
    items: [
      {
        text: getI18nValue('auth'),
        callback: function(trello) {
          return authorize(trello);
        }
      }
    ]
  });

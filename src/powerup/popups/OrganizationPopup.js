import DepartmentsPopup from './DepartmentsPopup';
import TicketViewsPopup from './TicketViewsPopup';

import AttachTicketView from '../attach/AttachTicketView';
import { getI18nValue } from '../../i18n/i18n';
import { getOrgs, getDepts } from '../api/api';
import { createPopup } from '../utils';
import { getState } from '../state';

export default (trello, callback = false) =>
  createPopup(trello, {
    title: getI18nValue('org.list'),
    items: (trello, options) =>
      getOrgs(trello)
        .then(({ data }) => {
          let { search } = options;
          let whichAttach = getState('whichAttach');
          let organizations = [];

          data
            .filter(orgInfo => {
              if (!search) {
                return true;
              }
              let { organizationName } = orgInfo;
              return (
                organizationName.toLowerCase().indexOf(search.toLowerCase()) !==
                -1
              );
            })
            .filter((_, index) => index < 10)
            .forEach(orgInfo => {
              let { organizationName, id: orgId } = orgInfo;

              organizations.push({
                text: organizationName,
                callback: trello => {
                  if (typeof callback === 'function') {
                    return callback(orgInfo);
                  }

                  return getDepts(orgId, trello).then(
                    ({ data: deptInfo, count: deptCount }) => {
                      if (deptCount) {
                        if (deptCount > 1) {
                          return DepartmentsPopup(orgInfo)(trello);
                        }
                        return TicketViewsPopup(
                          orgInfo,
                          deptInfo[0],
                          whichAttach === 'TICKET_VIEW_ATTACH'
                            ? AttachTicketView
                            : false
                        )(trello);
                      }
                    }
                  );
                }
              });
            });

          return organizations;
        })
        .catch(err => {
          throw err;
        }),
    search: {
      count: 10,
      placeholder: getI18nValue('search.portals'),
      empty: getI18nValue('no.portals.found'),
      searching: getI18nValue('fetching.portals'),
      debounce: 500
    }
  });

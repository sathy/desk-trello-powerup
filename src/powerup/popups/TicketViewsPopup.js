import TicketsPopup from './TicketsPopup';

import { getI18nValue } from '../../i18n/i18n';
import { getTicketViews } from '../api/api';
import { createPopup } from '../utils';
import { getInfoFromURI } from '../../utils';

export default (orgInfo, deptInfo, callback = false, timeZone) => trello =>
  createPopup(trello, {
    title: getI18nValue('ticket.view.list'),
    items: (trello, options) => {
      let { id: orgId } = orgInfo;
      let { id: deptId } = deptInfo;
      let { search } = options;

      return getTicketViews(orgId, deptId, trello)
        .then(({ data }) =>
          trello.card('attachments').then(({ attachments }) => {
            let attachedViews = attachments
              .filter(
                ({ url }) =>
                  url.includes(`${window.location.origin}/app/view.html`) &&
                  !url.includes('isTicket')
              )
              .map(({ url }) => {
                let { orgId, deptId, viewId } = getInfoFromURI(url);
                return `${orgId}.${deptId}.${viewId}`;
              });

            let views = [];

            data
              .filter(viewInfo => {
                let { name, id } = viewInfo;
                if (!search) {
                  if (
                    typeof callback === 'function' &&
                    attachedViews.includes(`${orgId}.${deptId}.${id}`)
                  ) {
                    return false;
                  }
                  return true;
                }

                if (
                  typeof callback === 'function' &&
                  attachedViews.includes(`${orgId}.${deptId}.${id}`)
                ) {
                  return false;
                }
                return name.toLowerCase().indexOf(search.toLowerCase()) !== -1;
              })
              .filter((_, index) => index < 10)
              .forEach(viewInfo => {
                let { name } = viewInfo;
                views.push({
                  text: name,
                  callback:
                    typeof callback === 'function'
                      ? callback(orgInfo, deptInfo, viewInfo, timeZone)
                      : TicketsPopup(orgInfo, deptInfo, viewInfo, timeZone)
                });
              });
            return views;
          })
        )
        .catch(err => {
          throw err;
        });
    },
    search: {
      count: 10,
      placeholder: getI18nValue('search.views'),
      empty: getI18nValue('no.views.found'),
      searching: getI18nValue('fetching.views')
    }
  });

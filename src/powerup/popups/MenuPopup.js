import AuthorizedPopup from '../authorization/AuthorizedPopup';

import { createPopup, closePopup, getAllData, log } from '../utils';

import { icon } from '../manifest.json';

let ClearConfig, GetConfig;

//eslint-disable-next-line
if (__DEVELOPMENT__) {
  ClearConfig = {
    text: 'Clear Configuration (Only on dev mode)',
    callback: trello => {
      trello.remove('member', 'private', ['config', 'isAuthorized', 'oauth']);
      trello.remove('card', 'private', ['attachedViews', 'attachedTickets']);
      return closePopup(trello);
    }
  };

  GetConfig = {
    text: 'Print Config of current user (Only on dev mode)',
    callback: trello => {
      getAllData(trello).then(data => {
        log(data);
      });
    }
  };
}

export default trello => {
  let isMemberSignedIn = trello.isMemberSignedIn();
  let isMemberCanWriteOnCard = trello.memberCanWriteToModel('card');
  return isMemberSignedIn && isMemberCanWriteOnCard
    ? [
        {
          icon: `${window.location.origin}${icon}`,
          text: 'Zoho Desk',
          callback: trello =>
            createPopup(trello, {
              title: 'Zoho Desk',
              items: () => {
                let Popup = AuthorizedPopup;

                //eslint-disable-next-line
                if (__DEVELOPMENT__) {
                  Popup = Popup.slice();
                  Popup.push(ClearConfig);
                  Popup.push(GetConfig);
                }

                return Popup;
              }
            })
        }
      ]
    : [];
};

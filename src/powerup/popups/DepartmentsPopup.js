import TicketViewsPopup from './TicketViewsPopup';
import AttachTicketView from '../attach/AttachTicketView';

import { getI18nValue } from '../../i18n/i18n';
import { getDepts } from '../api/api';
import { createPopup } from '../utils';
import { getState } from '../state';

export default (orgInfo, callback = false) => trello =>
  createPopup(trello, {
    title: getI18nValue('dept.list'),
    items: (trello, options) => {
      let { id: orgId } = orgInfo;
      let whichAttach = getState('whichAttach');
      return getDepts(orgId, trello)
        .then(({ data, timeZone }) => {
          let { search } = options;
          let departments = [];

          data
            .filter(deptInfo => {
              if (!search) {
                return true;
              }
              let { name } = deptInfo;
              return name.toLowerCase().indexOf(search.toLowerCase()) !== -1;
            })
            .filter((_, index) => index < 10)
            .forEach(deptInfo => {
              let { name } = deptInfo;
              departments.push({
                text: name,
                callback:
                  typeof callback === 'function'
                    ? callback(orgInfo, deptInfo)
                    : TicketViewsPopup(
                        orgInfo,
                        deptInfo,
                        whichAttach === 'TICKET_VIEW_ATTACH'
                          ? AttachTicketView
                          : false,
                        timeZone
                      )
              });
            });
          return departments;
        })
        .catch(err => {
          throw err;
        });
    },
    search: {
      count: 10,
      placeholder: getI18nValue('search.depts'),
      empty: getI18nValue('no.depts.found'),
      searching: getI18nValue('fetching.depts')
    }
  });

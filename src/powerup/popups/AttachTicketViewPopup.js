import OrganizationPopup from './OrganizationPopup';
import DepartmentsPopup from './DepartmentsPopup';
import TicketViewsPopup from './TicketViewsPopup';

import AuthorizePopup from '../authorization/AuthorizePopup';

import AttachTicketView from '../attach/AttachTicketView';
import { getData } from '../utils';
import { setState } from '../state';
import { getOrgs, getDepts } from '../api/api';
import { getI18nValue } from '../../i18n/i18n';

export default {
  text: getI18nValue('attach.ticket.views'),
  callback: trello => {
    setState('whichAttach', 'TICKET_VIEW_ATTACH');
    return getData(trello, 'isAuthorized', false).then(isAuthorized => {
      if (isAuthorized) {
        return getData(trello, 'config', {}).then(config => {
          let { orgInfo, deptInfo } = config;
          let { id: orgId } = orgInfo || {};
          let { id: deptId } = deptInfo || {};

          if (orgId && deptId) {
            return TicketViewsPopup(orgInfo, deptInfo, AttachTicketView)(
              trello
            );
          } else if (orgId) {
            return getDepts(orgId, trello).then(
              ({ data: deptInfo, count: deptCount }) => {
                if (deptCount && deptCount === 1) {
                  return TicketViewsPopup(
                    orgInfo,
                    deptInfo[0],
                    AttachTicketView
                  )(trello);
                }
                return DepartmentsPopup(orgInfo)(trello);
              }
            );
          }

          return getOrgs(trello).then(({ data: orgInfo, count: orgCount }) => {
            if (orgCount && orgCount === 1) {
              let [{ id: orgId }] = orgInfo;
              return getDepts(orgId, trello).then(
                ({ data: deptInfo, count: deptCount }) => {
                  if (deptCount && deptCount === 1) {
                    return TicketViewsPopup(
                      orgInfo[0],
                      deptInfo[0],
                      AttachTicketView
                    )(trello);
                  }
                  return DepartmentsPopup(orgInfo[0])(trello);
                }
              );
            }
            return OrganizationPopup(trello);
          });
        });
      }
      return AuthorizePopup(trello);
    });
  }
};

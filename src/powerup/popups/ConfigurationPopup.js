import AuthorizePopup from '../authorization/AuthorizePopup';

import { setState } from '../state';
import { getData } from '../utils';
import { getI18nValue } from '../../i18n/i18n';

export default {
  text: getI18nValue('config'),
  callback: trello => {
    setState('whichAttach', '');

    return getData(trello, 'isAuthorized', false).then(isAuthorized => {
      if (isAuthorized) {
        return trello.popup({
          title: getI18nValue('config.popup'),
          url: `${window.location.origin}/app/configuration.html`,
          height: 215
        });
      }
      return AuthorizePopup(trello);
    });
  }
};

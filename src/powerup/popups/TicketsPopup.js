import AttachTicket from '../attach/AttachTicket';
import { getTickets, searchTickets } from '../api/api';
import { createPopup } from '../utils';
import { getInfoFromURI } from '../../utils';

import { getI18nValue } from '../../i18n/i18n';

export default (orgInfo, deptInfo, viewInfo, timeZone) => trello =>
  createPopup(trello, {
    title: getI18nValue('tickets.list'),
    items: (trello, options) => {
      let { id: orgId } = orgInfo;
      let { id: deptId } = deptInfo;
      let { id: viewId } = viewInfo;
      let { search } = options;

      let APIUtil = search && search.length >= 3 ? searchTickets : getTickets;

      return APIUtil(
        orgId,
        deptId,
        viewId,
        trello,
        search.length >= 3 ? search : 10
      )
        .then(({ data }) =>
          trello.card('attachments').then(({ attachments }) => {
            let attachedTickets = attachments
              .filter(
                ({ url }) =>
                  url.includes(`${window.location.origin}/app/view.html`) &&
                  url.includes('isTicket')
              )
              .map(({ url }) => {
                let { orgId, deptId, viewId, ticketId } = getInfoFromURI(url);
                return `${orgId}.${deptId}.${viewId}.${ticketId}`;
              });

            let tickets = [];

            data
              .filter(ticket => {
                let { id } = ticket;
                return !attachedTickets.includes(
                  `${orgId}.${deptId}.${viewId}.${id}`
                );
              })
              .forEach(ticketInfo => {
                let { subject, ticketNumber } = ticketInfo;
                tickets.push({
                  text: `#${ticketNumber} ${subject}`,
                  callback: AttachTicket(
                    orgInfo,
                    deptInfo,
                    viewInfo,
                    ticketInfo,
                    timeZone
                  )
                });
              });
            return tickets;
          })
        )
        .catch(err => {
          throw err;
        });
    },
    search: {
      count: 10,
      placeholder: getI18nValue('search.tickets'),
      empty: getI18nValue('no.tickets.found'),
      searching: getI18nValue('fetching.tickets'),
      debounce: 500
    }
  });

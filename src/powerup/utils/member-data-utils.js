export let setDatum = (trello, datum) => {
  trello.set('member', 'private', datum);
};

export let setData = (trello, key, value, scope = 'member') =>
  trello.set(scope, 'private', key, value);

export let getData = (trello, key, defaultValue, scope = 'member') =>
  trello.get(scope, 'private', key, defaultValue);

export let removeData = (trello, key) =>
  trello.remove('member', 'private', key);

export let getAllData = trello => trello.getAll();

export let getToken = trello => {
  let { member: token } = trello.getContext();
  return token;
};

export let setAttachedViews = (trello, orgId, deptId, viewId) =>
  getData(trello, 'attachedViews', [], 'card').then(attachedViews =>
    setData(
      trello,
      'attachedViews',
      [...attachedViews, `${orgId}.${deptId}.${viewId}`],
      'card'
    )
  );

export let setAttachedTickets = (trello, orgId, deptId, viewId, ticketId) =>
  getData(trello, 'attachedTickets', [], 'card').then(attachedTickets =>
    setData(
      trello,
      'attachedTickets',
      [...attachedTickets, `${orgId}.${deptId}.${viewId}.${ticketId}`],
      'card'
    )
  );

let playground = console;

export let log = (...info) => {
  playground.log(...info);
};

export * from './member-data-utils';
export * from './popup-utils';

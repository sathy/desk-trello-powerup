export let createPopup = (trello, popupInfo) =>
  trello.popup(typeof popupInfo === 'function' ? popupInfo() : popupInfo);

export let closePopup = trello => {
  trello.closePopup();
};

import { get, set } from 'lodash-es';

let state = {};

export let getState = path => (path ? get(state, path) : state);

export let setState = (path, value) => {
  let nextState = set({}, path, value);
  state = Object.assign({}, state, nextState);
  return true;
};

export let clearState = () => {
  state = {};
};

//eslint-disable-next-line
if (__DEVELOPMENT__) {
  window.getState = getState;
}

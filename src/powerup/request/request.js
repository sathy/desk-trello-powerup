/* eslint-disable prefer-destructuring */
import { log } from '../utils';

// let XMLHttpRequest = window.XMLHttpRequest;
export let fetch = window.fetch;

window.XMLHttpRequest = function() {
  log('You can\'t use XMLHttpRequest API 😉');
};

window.fetch = () => {
  log('You can\'t use fetch API 😉');
};

let playground = console;

module.exports = {
  log: (...args) => {
    playground.log(...args);
  },
  request: require('./request')
};

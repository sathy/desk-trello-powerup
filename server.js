const fs = require('fs');
const path = require('path');
const url = require('url');
const cluster = require('cluster');
const { cpus, hostname } = require('os');
const express = require('express');
const https = require('https');
const webpack = require('webpack');
const devMiddleware = require('webpack-dev-middleware');
const compression = require('compression');

const requestHandlers = require('./request-handlers/index.js');
const { log } = require('./utils/index');

let { DESK_TRELLO_HOST_NAME, DESK_TRELLO_PORT } = process.env;

let isDev = process.env.npm_config_dev_mode ? true : false;
let excludeURLs = [
  '/api/v1/auth',
  '/api/v1/token',
  '/api/slack',
  '/api/slackCustomAction'
];

if (cluster.isMaster && !isDev) {
  let numWorkers = cpus().length;

  log(`Master cluster setting up ${numWorkers} workers...`);

  for (let i = 0; i < numWorkers; i++) {
    cluster.fork();
  }

  cluster.on('online', worker => {
    log(`Worker ${worker.process.pid} is online`);
  });

  cluster.on('exit', (worker, code, signal) => {
    log(
      `Worker ${
        worker.process.pid
      } died with code: ${code}, and signal: ${signal}`
    );
    log('Starting a new worker');
    cluster.fork();
  });
} else {
  let app = express();

  app.use(compression());

  if (isDev) {
    const config = require('./webpack.config')('development');
    let compiler = webpack(config);
    app.use(devMiddleware(compiler, { publicPath: '/app' }));
  }

  let allowedOrigins = ['trello.com', 'slack.com'];

  app.use((req, res, next) => {
    log(
      `Worker \x1b[32m${process.pid}\x1b[0m is processing this(\x1b[32m${
        req.url
      }\x1b[0m) request`
    );

    let host = req.get('Host');

    if (allowedOrigins.includes(host)) {
      res.setHeader('Access-Control-Allow-Origin', `https://${host}`);
    }
    if (isDev) {
      res.setHeader('Cache-Control', 'no-cache');
    }

    let { pathname } = url.parse(req.url);
    if (pathname.includes('/api') && !excludeURLs.includes(pathname)) {
      let parent = req.get('PARENT_ORIGIN');
      if (
        host ===
          (isDev
            ? `${hostname()}.tsi.zohocorpin.com:6060`
            : 'trello.zohodesk.services') &&
        parent === 'https://trello.com/'
      ) {
        next();
      } else {
        log(req.url, 'Unauthenticated URL');
        res.status(200).end('Requested URL not found');
      }
    } else {
      next();
    }
  });

  app.use('/icon', (req, res) => {
    let { color } = req.query;
    if (color) {
      res.sendFile(path.join(__dirname, './app/images/desk-logo-grey.png'));
    } else {
      res.sendFile(path.join(__dirname, './app/images/desk-logo.png'));
    }
  });

  app.use('/app', express.static('app'));

  app.listen(6565, err => {
    if (err) {
      throw err;
    }
    if (isDev) {
      log(`Listening at http://${hostname()}:6565/app`);
    }
  });

  https
    .createServer(
      isDev
        ? {
            key: fs.readFileSync(path.join(__dirname, './cert/dev/key.pem')),
            cert: fs.readFileSync(path.join(__dirname, './cert/dev/cert.pem')),
            passphrase: 'zddqa1585f82'
          }
        : {
            key: fs.readFileSync(path.join(__dirname, './cert/prod/key.pem')),
            cert: fs.readFileSync(path.join(__dirname, './cert/prod/cert.pem'))
          },
      app
    )
    .listen(isDev ? 6060 : DESK_TRELLO_PORT, err => {
      if (err) {
        throw err;
      }
      if (!isDev) {
        log(
          `Listening at https://${DESK_TRELLO_HOST_NAME}:${DESK_TRELLO_PORT}/app`
        );
      } else if (isDev) {
        log(`Listening at https://${hostname()}:6060/app`);
      }
    });

  requestHandlers(app);
}
